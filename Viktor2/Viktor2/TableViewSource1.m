//
//  TableViewSource1.m
//  Viktor2
//
//  Created by Denis Voronov on 17/02/16.
//  Copyright © 2016 emanor. All rights reserved.
//

#import "TableViewSource1.h"

@interface TableViewSource1()

@property (nonatomic, strong) NSArray* tableData;

@end


@implementation TableViewSource1

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tableData = @[
                           @{@"name":@"Alex"},
                           @{@"name":@"Max"},
                           @{@"name":@"Andrew"},
                           ];
    }
    return self;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    if (cell) {
        cell.textLabel.text = self.tableData[indexPath.row][@"name"];
        cell.backgroundColor = [UIColor lightGrayColor];
    }
    return cell;
}


@end
