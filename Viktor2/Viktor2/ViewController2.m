//
//  ViewController2.m
//  Viktor2
//
//  Created by Denis Voronov on 10/12/15.
//  Copyright © 2015 emanor. All rights reserved.
//

#import "ViewController2.h"
#import "TableViewSource1.h"
#import "TableViewSource2.h"

@interface ViewController2 ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) TableViewSource1* dataSource1;
@property (nonatomic, strong) TableViewSource2* dataSource2;

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];

    self.dataSource1 = [TableViewSource1 new];
    self.dataSource2 = [TableViewSource2 new];
    
    self.tableView.dataSource = self.dataSource1;
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentedControlValueChanged:(id)sender {
    UISegmentedControl* control = (UISegmentedControl*)sender;
    if (control.selectedSegmentIndex == 0) {
        self.tableView.dataSource = self.dataSource1;
    }
    else {
        self.tableView.dataSource = self.dataSource2;
    }
    
    [self.tableView reloadData];
}

@end
