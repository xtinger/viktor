//
//  AppDelegate.m
//  Viktor2
//
//  Created by Denis Voronov on 07/12/15.
//  Copyright © 2015 emanor. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "UIColor+Flat.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

#define COLOR_RGBA(r,g,b,a) [UIColor colorWithRed:(float)r/255 green:(float)g/255 blue:(float)b/255 alpha:(float)a/100]

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    MainViewController* mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:mainViewController];
    navVC.navigationBar.translucent = NO;
    navVC.navigationBar.tintColor = [UIColor greenColor];
    self.window.rootViewController = navVC;
    [self.window makeKeyAndVisible];
    
//    [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:50.0f/255 green:20.0f/255 blue:150.0f/255 alpha:1];
    
//    [UINavigationBar appearance].barTintColor = COLOR_RGBA(250, 20, 150, 100);
    
    [UINavigationBar appearance].barTintColor = [UIColor flat:Madison];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
