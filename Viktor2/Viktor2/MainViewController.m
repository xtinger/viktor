//
//  MainViewController.m
//  Viktor2
//
//  Created by Denis Voronov on 07/12/15.
//  Copyright © 2015 emanor. All rights reserved.
//

#import "MainViewController.h"
#import "ViewController2.h"

@implementation MainViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = [NSString stringWithFormat:@"Navigation controller %ld", (long)self.navigationController.viewControllers.count];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"ABC" style:UIBarButtonItemStylePlain target:self action:@selector(rightAction)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Назад на %ld", (long)self.navigationController.viewControllers.count] style:UIBarButtonItemStylePlain target:self action:nil];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"%@",self.navigationController.viewControllers);
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (IBAction)buttonTouched:(id)sender {
    MainViewController* vc = [MainViewController new];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)pushButtonTouched:(id)sender {
    ViewController2* vc = [ViewController2 new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)closePresentedButtonTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) rightAction {
    self.view.backgroundColor = [UIColor redColor];
}

@end
