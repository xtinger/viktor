//
//  TableViewSource1.m
//  Viktor2
//
//  Created by Denis Voronov on 17/02/16.
//  Copyright © 2016 emanor. All rights reserved.
//

#import "TableViewSource2.h"

@interface TableViewSource2()

@property (nonatomic, strong) NSArray* tableData;

@end


@implementation TableViewSource2

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tableData = @[
                           @{@"name":@"Alex2"},
                           @{@"name":@"Max2"},
                           @{@"name":@"Andrew2"},
                           ];
    }
    return self;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    if (cell) {
        cell.textLabel.text = self.tableData[indexPath.row][@"name"];
        cell.backgroundColor = [UIColor lightGrayColor];
    }
    return cell;
}


@end
