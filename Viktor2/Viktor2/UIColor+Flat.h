//
//  UIColor+Flat.h
//  SDevFlatColorsObj
//
//  Created by Sedat Ciftci on 23/05/15.
//  Copyright (c) 2015 Sedat Ciftci. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlatColors.h"

@interface UIColor (Flat)
+ (UIColor *) flat:(Colors) color;
@end
