//
//  TableViewSource1.h
//  Viktor2
//
//  Created by Denis Voronov on 17/02/16.
//  Copyright © 2016 emanor. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface TableViewSource2 : NSObject<UITableViewDataSource>

@property (nonatomic, strong, readonly) NSArray* tableData;

@end
