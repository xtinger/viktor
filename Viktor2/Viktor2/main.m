//
//  main.m
//  Viktor2
//
//  Created by Denis Voronov on 07/12/15.
//  Copyright © 2015 emanor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
