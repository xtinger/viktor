//
//  AppDelegate.h
//  Viktor1
//
//  Created by Denis Voronov on 01/12/15.
//  Copyright © 2015 emanor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

