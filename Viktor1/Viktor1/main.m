//
//  main.m
//  Viktor1
//
//  Created by Denis Voronov on 01/12/15.
//  Copyright © 2015 emanor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
