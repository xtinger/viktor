//
//  SomeObject.m
//  Viktor1
//
//  Created by Denis Voronov on 01/12/15.
//  Copyright © 2015 emanor. All rights reserved.
//

#import "SomeObject.h"

@implementation SomeObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initialize:10];
    }
    return self;
}

- (instancetype)initWithInteger :(int)someInteger
{
    self = [super init];
    if (self) {
        [self initialize:someInteger];
    }
    return self;
}

- (void) initialize :(int)someInteger {
    self.integerProperty = someInteger;
    NSLog(@"integerProperty = %ld", (long)self.integerProperty);
}

- (void) myMethod {
    
    BOOL allIsOk = 1 > 0; //NO
    int integer = 10;
    NSString* string = @"string";
    char* str = "c string";
    
    int x = integer > 10 ? 100 : 99;
    
    NSLog(@"allIsOk = %@", allIsOk ? @"true" : @"false");
    NSLog(@"%d", integer);
    NSLog(@"%@", string);
    NSLog(@"%s", str);
    
    if (allIsOk) {
    }
    else {
    }
    
    switch (integer) {
        case 1:
            NSLog(@"%d", 1);
            break;
        default:
            break;
    }
    
    for (int i = 0; i < 100; i++) {
        NSLog(@"%d", i);
    }
    
    NSNumber* number = @(1);
    NSInteger int1 = [number integerValue];
    
    SomeObject* someObject = [[SomeObject alloc] initWithInteger:99];
    
    NSArray* array = [[NSArray alloc] init];
    NSArray* array1 = @[];
    NSArray* array2 = [NSArray new];
    NSArray* array3 = [NSArray array];
    NSArray* array4 = [NSArray arrayWithObjects:@"1", @(int1), nil];
    NSArray* array5 = @[@"1", @(int1), someObject];
    
    for (int i = 0; i < array5.count; i++) {
        NSLog(@"%@", array5[i]);
    }
    
    NSLog(@"-------");
    for (NSObject* object in array5) {
        NSLog(@"%@", object);
    }
    
}

- (void) lesson2 {
    NSArray* array = [[NSArray alloc] init];
    NSString* arrayClassName = NSStringFromClass([array class]);
    NSLog(@"%@", arrayClassName);
    
    Class class = NSClassFromString(@"NSArray");
    [self classMethod:class];
    
    NSLog(@"%u", [self randomIntegerFrom:2 to:5]);
}

- (void) classMethod :(Class) class {
    NSLog(@"%@", class);
}

- (u_int32_t) randomIntegerFrom :(u_int32_t)from to:(u_int32_t)to {
    u_int32_t random = arc4random_uniform(to - from) + from;
    return random;
}

- (NSString*) description {
    return [NSString stringWithFormat:@"SomeObject with %ld", (long)self.integerProperty];
}

@end
