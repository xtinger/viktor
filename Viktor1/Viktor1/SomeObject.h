//
//  SomeObject.h
//  Viktor1
//
//  Created by Denis Voronov on 01/12/15.
//  Copyright © 2015 emanor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SomeObject : NSObject

@property (nonatomic) NSInteger integerProperty;

- (instancetype)initWithInteger :(int)someInteger;
- (void) myMethod;
- (void) lesson2;

@end
